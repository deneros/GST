# -*- coding: utf-8 -*-
from odoo import models, fields, api, SUPERUSER_ID, _
from odoo.exceptions import UserError

class purchaseorder(models.Model):
    _inherit = 'purchase.order'

    READONLY_STATES = {
        'purchase': [('readonly', True)],
        'done': [('readonly', True)],
        'cancel': [('readonly', True)],
    }

    tax_type = fields.Selection([
        ('gst', 'GST'),
        ('igst', 'IGST'),
    ], default='gst', string="Tax Type", states=READONLY_STATES)

    @api.onchange('tax_type')
    def _onchange_tax_type(self):
        for order in self:
            if not order.tax_type:
               raise UserError(
                        _('Please Select tax Type.'))
            for line in order.order_line:
                line.tax_type = order.tax_type
                line._onchange_tax_type()
        self._amount_all()

    @api.depends('order_line.price_total')
    def _compute_gst_amount(self):
        for order in self:
            amount_tax = 0.0
            for line in order.order_line:
                if order.company_id.tax_calculation_rounding_method == 'round_globally':
                    taxes = line.taxes_id.compute_all(line.price_unit, line.order_id.currency_id, line.product_qty, product=line.product_id, partner=line.order_id.partner_id)
                    amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
                else:
                    amount_tax += line.price_tax
            order.update({
                'sgst_amount_tax': order.tax_type == 'gst' and (order.currency_id.round(amount_tax / 2)) or 0.0,
                'cgst_amount_tax': order.tax_type == 'gst' and (order.currency_id.round(amount_tax / 2)) or 0.0,
                'igst_amount_tax': order.tax_type == 'igst' and (order.currency_id.round(amount_tax)) or 0.0,
            })

    sgst_amount_tax = fields.Monetary(string='SGST Tax',
        store=True, readonly=True, compute='_compute_gst_amount')
    cgst_amount_tax = fields.Monetary(string='CGST Tax',
        store=True, readonly=True, compute='_compute_gst_amount')
    igst_amount_tax = fields.Monetary(string='IGST Tax',
        store=True, readonly=True, compute='_compute_gst_amount')


class purchaseorderline(models.Model):
    _inherit = 'purchase.order.line'

    @api.depends('product_qty', 'price_unit', 'taxes_id')
    def _amount_gst_line(self):
        for line in self:
            taxes = line.taxes_id.compute_all(line.price_unit, line.order_id.currency_id, line.product_qty, product=line.product_id, partner=line.order_id.partner_id)
            line.update({
                'sgst_amount_tax_line': line.tax_type == 'gst' and ((taxes['total_included'] - taxes['total_excluded']) / 2) or 0.0,
                'cgst_amount_tax_line': line.tax_type == 'gst' and ((taxes['total_included'] - taxes['total_excluded']) / 2) or 0.0,
                'igst_amount_tax_line': line.tax_type == 'igst' and (taxes['total_included'] - taxes['total_excluded']) or 0.0,
            })

    tax_type = fields.Selection([
        ('gst', 'GST'),
        ('igst', 'IGST'),
    ], default='gst', string="Tax Type")
    sgst_amount_tax_line = fields.Monetary(string='SGST Tax',
        store=True, readonly=True, compute='_amount_gst_line')
    cgst_amount_tax_line = fields.Monetary(string='CGST Tax',
        store=True, readonly=True, compute='_amount_gst_line')
    igst_amount_tax_line = fields.Monetary(string='IGST Tax',
        store=True, readonly=True, compute='_amount_gst_line')

    @api.onchange('tax_type')
    def _onchange_tax_type(self):
        fpos = self.order_id.fiscal_position_id
        if self.tax_type == 'gst':
            if self.env.uid == SUPERUSER_ID:
                company_id = self.env.user.company_id.id
                self.taxes_id = fpos.map_tax(self.product_id.supplier_taxes_id.filtered(lambda r: r.company_id.id == company_id))
            else:
                self.taxes_id = fpos.map_tax(self.product_id.supplier_taxes_id)
        else:
            if self.env.uid == SUPERUSER_ID:
                company_id = self.env.user.company_id.id
                self.taxes_id = fpos.map_tax(self.product_id.igst_supplier_taxes_id.filtered(lambda r: r.company_id.id == company_id))
            else:
                self.taxes_id = fpos.map_tax(self.product_id.igst_supplier_taxes_id)
