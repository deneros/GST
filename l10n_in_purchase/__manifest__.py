# -*- coding: utf-8 -*-
{
    'name': 'Purchase changes related to Indian GST only',
    'version': '1.0',
    'description': """Purchase changes related to Indian GST only""",
    'category': 'Localization',
    'depends': [
        'purchase',
    ],
    'data': [
        'views/order_view.xml',
    ],
    'installable': True,
    'application': True,
}
