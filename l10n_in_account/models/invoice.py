# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError

class accountinvoice(models.Model):
    _inherit = 'account.invoice'

    tax_type = fields.Selection([
        ('gst', 'GST'),
        ('igst', 'IGST'),
    ], default='gst', string="Tax Type", states={'draft': [('readonly', False)]})

    @api.onchange('tax_type')
    def _onchange_tax_type(self):
        for invoice in self:
            if not invoice.tax_type:
               raise UserError(
                        _('Please Select tax Type.'))
            for line in invoice.invoice_line_ids:
                line.tax_type = invoice.tax_type
                line._onchange_tax_type()
        self._compute_amount()


    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice', 'type')
    def _compute_gst_amount(self):
        self.sgst_amount_tax = self.tax_type == 'gst' and (sum(line.amount for line in self.tax_line_ids) / 2) or 0.0
        self.cgst_amount_tax = self.tax_type == 'gst' and (sum(line.amount for line in self.tax_line_ids) / 2) or 0.0
        self.igst_amount_tax = self.tax_type == 'igst' and (sum(line.amount for line in self.tax_line_ids)) or 0.0

    sgst_amount_tax = fields.Monetary(string='SGST Tax',
        store=True, readonly=True, compute='_compute_gst_amount')
    cgst_amount_tax = fields.Monetary(string='CGST Tax',
        store=True, readonly=True, compute='_compute_gst_amount')
    igst_amount_tax = fields.Monetary(string='IGST Tax',
        store=True, readonly=True, compute='_compute_gst_amount')


class accountinvoiceline(models.Model):
    _inherit = 'account.invoice.line'

    tax_type = fields.Selection([
        ('gst', 'GST'),
        ('igst', 'IGST'),
    ], default='gst', string="Tax Type")

    @api.onchange('tax_type')
    def _onchange_tax_type(self):
        self._set_taxes()

    def _set_taxes(self):
        """ Used in on_change to set taxes and price."""
        if self.invoice_id.type in ('out_invoice', 'out_refund'):
            if self.tax_type == 'gst':
                taxes = self.product_id.taxes_id  or self.account_id.tax_ids
            else:
                taxes = self.product_id.igst_taxes_id or self.account_id.tax_ids
        else:
            if self.tax_type == 'gst':
                taxes = self.product_id.supplier_taxes_id or self.account_id.tax_ids
            else:
                taxes = self.product_id.igst_supplier_taxes_id or self.account_id.tax_ids

        # Keep only taxes of the company
        company_id = self.company_id or self.env.user.company_id
        taxes = taxes.filtered(lambda r: r.company_id == company_id)

        self.invoice_line_tax_ids = fp_taxes = self.invoice_id.fiscal_position_id.map_tax(taxes, self.product_id, self.invoice_id.partner_id)

        fix_price = self.env['account.tax']._fix_tax_included_price
        if self.invoice_id.type in ('in_invoice', 'in_refund'):
            prec = self.env['decimal.precision'].precision_get('Product Price')
            if not self.price_unit or float_compare(self.price_unit, self.product_id.standard_price, precision_digits=prec) == 0:
                self.price_unit = fix_price(self.product_id.standard_price, taxes, fp_taxes)
        else:
            self.price_unit = fix_price(self.product_id.lst_price, taxes, fp_taxes)
