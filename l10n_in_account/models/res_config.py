# -*- coding: utf-8 -*-

import odoo
from odoo import SUPERUSER_ID
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'

    igst_sale_tax_id = fields.Many2one('account.tax.template', string='Default IGST sale tax', oldname="igst_sale_tax")
    igst_purchase_tax_id = fields.Many2one('account.tax.template', string='Default IGST purchase tax', oldname="igst_purchase_tax")
    default_igst_sale_tax_id = fields.Many2one('account.tax', string="Default IGST Sale Tax", help="This sale tax will be assigned by default on new products.", oldname="default_igst_sale_tax")
    default_igst_purchase_tax_id = fields.Many2one('account.tax', string="Default IGST Purchase Tax", help="This purchase tax will be assigned by default on new products.", oldname="default_igst_purchase_tax")

    # def get_default_igst_sale_tax_id(self, cr, uid, fields, context=None):
    #     icp = self.pool.get('ir.config_parameter')
    #     tax_id = safe_eval(icp.get_param(
    #         cr, uid,
    #         'l10n_in_account.igst_sale_tax_id',
    #         'False'
    #     ))
    #     return {
    #         'igst_sale_tax_id': tax_id and int(tax_id) or False,
    #     }
    # 
    # def set_default_igst_sale_tax_id(self, cr, uid, ids, context=None):
    #     config = self.browse(cr, uid, ids[0], context=context)
    #     icp = self.pool.get('ir.config_parameter')
    #     icp.set_param(
    #         cr, openerp.SUPERUSER_ID,
    #         'l10n_in_account.igst_sale_tax_id',
    #         repr(config.igst_sale_tax_id.id)
    #     )
    # 
    # def get_default_igst_purchase_tax_id(self, cr, uid, fields, context=None):
    #     icp = self.pool.get('ir.config_parameter')
    #     tax_id = safe_eval(icp.get_param(
    #         cr, uid,
    #         'l10n_in_account.igst_purchase_tax_id',
    #         'False'
    #     ))
    #     return {
    #         'igst_purchase_tax_id': tax_id and int(tax_id) or False,
    #     }
    # 
    # def set_default_igst_purchase_tax_id(self, cr, uid, ids, context=None):
    #     config = self.browse(cr, uid, ids[0], context=context)
    #     icp = self.pool.get('ir.config_parameter')
    #     icp.set_param(
    #         cr, openerp.SUPERUSER_ID,
    #         'l10n_in_account.igst_purchase_tax_id',
    #         repr(config.igst_purchase_tax_id.id)
    #     )

    @api.onchange('company_id')
    def onchange_company_id(self):
        res = super(AccountConfigSettings, self).onchange_company_id()
        if self.company_id:
            company = self.company_id
            ir_values = self.env['ir.values']
            igst_taxes_id = ir_values.get_default('product.template', 'igst_taxes_id', company_id = self.company_id.id)
            igst_supplier_taxes_id = ir_values.get_default('product.template', 'igst_supplier_taxes_id', company_id = self.company_id.id)
            self.default_igst_sale_tax_id = isinstance(igst_taxes_id, list) and len(igst_taxes_id) > 0 and igst_taxes_id[0] or igst_taxes_id
            self.default_igst_purchase_tax_id = isinstance(igst_supplier_taxes_id, list) and len(igst_supplier_taxes_id) > 0 and igst_supplier_taxes_id[0] or igst_supplier_taxes_id
        return res

    @api.multi
    def set_product_taxes(self):
        """ Set the product taxes if they have changed """
        res = super(AccountConfigSettings, self).set_product_taxes()
        ir_values_obj = self.env['ir.values']
        if self.igst_sale_tax_id:
            ir_values_obj.sudo().set_default('product.template', "igst_taxes_id", [self.default_igst_sale_tax_id.id], for_all_users=True, company_id=self.company_id.id)
        if self.igst_purchase_tax_id:
            ir_values_obj.sudo().set_default('product.template', "igst_supplier_taxes_id", [self.default_igst_purchase_tax_id.id], for_all_users=True, company_id=self.company_id.id)
        return res

    @api.multi
    def set_chart_of_accounts(self):
        """ install a chart of accounts for the given company (if required) """
        if self.chart_template_id and not self.has_chart_of_accounts and self.expects_chart_of_accounts:
            if self.company_id.chart_template_id and self.chart_template_id != self.company_id.chart_template_id:
                raise UserError(_('You can not change a company chart of account once it has been installed'))
            wizard = self.env['wizard.multi.charts.accounts'].create({
                'company_id': self.company_id.id,
                'chart_template_id': self.chart_template_id.id,
                'transfer_account_id': self.template_transfer_account_id.id,
                'code_digits': self.code_digits or 6,
                'sale_tax_id': self.sale_tax_id.id,
                'purchase_tax_id': self.purchase_tax_id.id,
                'igst_sale_tax_id': self.igst_sale_tax_id.id,
                'igst_purchase_tax_id': self.igst_purchase_tax_id.id,
                'sale_tax_rate': self.sale_tax_rate,
                'purchase_tax_rate': self.purchase_tax_rate,
                'complete_tax_set': self.complete_tax_set,
                'currency_id': self.currency_id.id,
                'bank_account_code_prefix': self.bank_account_code_prefix or self.chart_template_id.bank_account_code_prefix,
                'cash_account_code_prefix': self.cash_account_code_prefix or self.chart_template_id.cash_account_code_prefix,
            })
            wizard.execute()


class WizardMultiChartsAccounts(models.TransientModel):
    _inherit = 'wizard.multi.charts.accounts'


    igst_sale_tax_id = fields.Many2one('account.tax.template', string='Default IGST Sales Tax')
    igst_purchase_tax_id = fields.Many2one('account.tax.template', string='Default IGst Purchase Tax')

    @api.model
    def default_get(self, fields):
        context = self._context or {}
        res = super(WizardMultiChartsAccounts, self).default_get(fields)
        tax_templ_obj = self.env['account.tax.template']
        account_chart_template = self.env['account.chart.template']
        chart_templates = account_chart_template.search([('visible', '=', True)])
        if chart_templates:
            chart_id = max(chart_templates.ids)
            if context.get("default_charts"):
                model_data = self.env['ir.model.data'].search_read([('model', '=', 'account.chart.template'), ('module', '=', context.get("default_charts"))], ['res_id'])
                if model_data:
                    chart_id = model_data[0]['res_id']
            chart = account_chart_template.browse(chart_id)
            chart_hierarchy_ids = self._get_chart_parent_ids(chart)
            if 'igst_sale_tax_id' in fields:
                sale_tax = tax_templ_obj.search([('chart_template_id', 'in', chart_hierarchy_ids),
                                                              ('type_tax_use', '=', 'sale')], limit=1, order='sequence')
                res.update({'igst_sale_tax_id': sale_tax and sale_tax.id or False})
            if 'igst_purchase_tax_id' in fields:
                purchase_tax = tax_templ_obj.search([('chart_template_id', 'in', chart_hierarchy_ids),
                                                                  ('type_tax_use', '=', 'purchase')], limit=1, order='sequence')
                res.update({'igst_purchase_tax_id': purchase_tax and purchase_tax.id or False})
        return res

    @api.multi
    def execute(self):
        context = self._context or {}
        res = super(WizardMultiChartsAccounts, self).execute()
        company = self.company_id
        acc_template_ref, taxes_ref = self.chart_template_id._install_template(company, code_digits=self.code_digits, transfer_account_id=self.transfer_account_id)
        if self.sale_tax_id and taxes_ref:
            ir_values_obj.sudo().set_default('product.template', "igst_taxes_id", [taxes_ref[self.igst_sale_tax_id.id]], for_all_users=True, company_id=company.id)
        if self.purchase_tax_id and taxes_ref:
            ir_values_obj.sudo().set_default('product.template', "igst_supplier_taxes_id", [taxes_ref[self.igst_purchase_tax_id.id]], for_all_users=True, company_id=company.id)
        return res
