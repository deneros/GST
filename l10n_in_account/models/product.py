# -*- coding: utf-8 -*-

from odoo import models, fields


class producttemplate(models.Model):
    _inherit = 'product.template'

    tax_type = fields.Selection([
        ('gst', 'GST'),
        ('igst', 'IGST'),
    ], default='gst', string="Defualt Tax Type")
    igst_taxes_id = fields.Many2many('account.tax',
        'product_taxes_igst_rel', 'prod_id', 'tax_id',
        string='IGST Customer Taxes',
        domain=[('type_tax_use', '=', 'sale')]
    )
    igst_supplier_taxes_id = fields.Many2many('account.tax',
        'product_supplier_igst_taxes_rel',
        'prod_id', 'tax_id',
        string='IGST Vendor Taxes',
        domain=[('type_tax_use', '=', 'purchase')]
    )
    property_account_igstincome_id = fields.Many2one('account.account', company_dependent=True,
        string="IGST Income Account", oldname="property_account_income",
        domain=[('deprecated', '=', False)],
    )
    property_account_igstexpense_id = fields.Many2one('account.account', company_dependent=True,
        string="IGST Expense Account", oldname="property_account_expense",
        domain=[('deprecated', '=', False)],
    )
