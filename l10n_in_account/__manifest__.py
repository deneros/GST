# -*- coding: utf-8 -*-
{
    'name': 'Account changes related to Indian GST only',
    'version': '1.0',
    'description': """GST Indian Accounting Changes""",
    'category': 'Localization',
    'depends': [
        'l10n_in',
        'base_vat'
    ],
    'data': [
        'data/account_fiscal_position_data.xml',
        'views/report_invoice.xml',
        'views/report_templates.xml',
        'views/res_company_view.xml',
        'views/res_partner_views.xml',
        'views/invoice_view.xml',
        'views/product_template_view.xml',
        'views/res_config_view.xml',
    ],
    'installable': True,
    'application': True,
}
