# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class saleorder(models.Model):
    _inherit = 'sale.order'

    tax_type = fields.Selection([
        ('gst', 'GST'),
        ('igst', 'IGST'),
    ], default='gst', string="Tax Type", states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})

    @api.onchange('tax_type')
    def _onchange_tax_type(self):
        for order in self:
            if not order.tax_type:
                raise UserError(
                        _('Please Select tax Type.'))
            for line in order.order_line:
                line.tax_type = order.tax_type
                line._onchange_tax_type()
        self._amount_gst_all()
        self.button_dummy()

    @api.depends('order_line.price_total')
    def _amount_gst_all(self):
        """
        Compute the total amounts of the SO.
        """
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                # FORWARDPORT UP TO 10.0
                if order.company_id.tax_calculation_rounding_method == 'round_globally':
                    price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
                    taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=order.partner_shipping_id)
                    amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
                else:
                    amount_tax += line.price_tax
            order.update({
                'sgst_amount_tax': order.tax_type == 'gst' and order.currency_id.round(amount_tax/ 2) or 0.0,
                'cgst_amount_tax': order.tax_type == 'gst' and order.currency_id.round(amount_tax / 2) or 0.0,
                'igst_amount_tax': order.tax_type == 'igst' and order.currency_id.round(amount_tax) or 0.0,
            })

    sgst_amount_tax = fields.Monetary(string='SGST Tax',
        store=True, readonly=True, compute='_amount_gst_all')
    cgst_amount_tax = fields.Monetary(string='CGST Tax',
        store=True, readonly=True, compute='_amount_gst_all')
    igst_amount_tax = fields.Monetary(string='IGST Tax',
        store=True, readonly=True, compute='_amount_gst_all')


class saleorderline(models.Model):
    _inherit = 'sale.order.line'

    @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id')
    def _amount_gst_line(self):
        for line in self:
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_shipping_id)
            line.update({
                'sgst_amount_tax_line': line.tax_type == 'gst' and (taxes['total_included'] - taxes['total_excluded']) / 2 or 0.0,
                'cgst_amount_tax_line': line.tax_type == 'gst' and (taxes['total_included'] - taxes['total_excluded']) / 2 or 0.0,
                'igst_amount_tax_line': line.tax_type == 'igst'and taxes['total_included'] - taxes['total_excluded'] or 0.0,
            })

    tax_type = fields.Selection([
        ('gst', 'GST'),
        ('igst', 'IGST'),
    ], default='gst', string="Tax Type")
    sgst_amount_tax_line = fields.Monetary(string='SGST Tax',
        store=True, readonly=True, compute='_amount_gst_line')
    cgst_amount_tax_line = fields.Monetary(string='CGST Tax',
        store=True, readonly=True, compute='_amount_gst_line')
    igst_amount_tax_line = fields.Monetary(string='IGST Tax',
        store=True, readonly=True, compute='_amount_gst_line')

    @api.onchange('tax_type')
    def _onchange_tax_type(self):
        self._compute_tax_id()

    @api.multi
    def _compute_tax_id(self):
        for line in self:
            fpos = line.order_id.fiscal_position_id or line.order_id.partner_id.property_account_position_id
            # If company_id is set, always filter taxes by the company
            if line.tax_type == 'gst':
                taxes = line.product_id.taxes_id.filtered(lambda r: not line.company_id or r.company_id == line.company_id)
            else:
                taxes = line.product_id.igst_taxes_id.filtered(lambda r: not line.company_id or r.company_id == line.company_id)
            line.tax_id = fpos.map_tax(taxes, line.product_id, line.order_id.partner_shipping_id) if fpos else taxes

