# -*- coding: utf-8 -*-
{
    'name': 'Sale changes related to Indian GST only',
    'version': '1.0',
    'description': """Sale changes related to Indian GST only""",
    'category': 'Localization',
    'depends': [
        'sale',
    ],
    'data': [
        'views/order_view.xml',
    ],
    'installable': True,
    'application': True,

}
